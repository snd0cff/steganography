from PIL import Image
import binascii
import os.path

def str2bin(message): #change string to binary 
	binary = bin(int(binascii.hexlify(message),16))
	return binary[2::]
def bin2str(binary): #change binary to string
	message = binascii.unhexlify('%x' % (int('0b'+binary,2)))
	return message

def clear_bit(xy,name,secret):  #clear last bit of this pixel witch will be used to hide secret
	im = Image.open(name)
	width,height=im.size
	im=im.convert('RGB')
	counter=0
	for x in range(0,width,10):
		if counter<=(len(secret)):
			for y in range(0,height,10):
				xy=(x,y)
				r,g,b=im.getpixel(xy)
				r=r&compare # clears last bit
				g=g&compare
				b=b&compare
				im.putpixel(xy,(r,g,b)) # puts to image changed pixel
				counter+=1
		else:
			break
	return im

def encode(xy,name,im,secret): # put a proper value of secret to pixels
	z=0
	width,height=im.size
	for x in range(0,width,10):
		for y in range(0,height,10):
			xy=(x,y)
			r,g,b=im.getpixel(xy)
			if z<len(secret):	# stops when secred ended
				if int(secret[z])==1:
					r+=1
					im.putpixel(xy,(r,g,b))
				z+=1
				tab_r.append(bin(r)[-1])
			if z<len(secret):
				if int(secret[z])==1:
					g+=1
					im.putpixel(xy,(r,g,b))
				z+=1
				tab_g.append(bin(g)[-1])
			if z<len(secret):
				if int(secret[z])==1:
					b+=1
					im.putpixel(xy,(r,g,b))
				z+=1
				tab_b.append(bin(b)[-1])
			#if z>=len(secret):
			#	print ("srututu")
			#	break
			if len(secret)>(width/10 * height/10): # check if secret is enough short to fulfil image
				print ("Secret is too long. Please shorten it or chose bigger image.")
				selection()
	if ".png" in name:
		im.save(name.replace(".png","-encoded.png"), "PNG") # save image as "name-encoded.png"
	else:
		extension = name.split(".")
		print("changing extension from " +str(extension[1])+ " to png")
		im.save(name.replace("."+extension[1],"-encoded.png"), "PNG")
	im.show()

def decode(): # function used to decode image
	proper_name=1
	while proper_name:
		imag = raw_input("Name of image: ")
		if os.path.isfile(imag): # check if file exist
			must=".png"
			if must in imag:
				im = Image.open(imag)
				width,height=im.size
				im=im.convert('RGB')
				decoding=[] # contain bits of secret
				for x in range(0,width,10):
					for y in range(0,height,10):
						xy=(x,y)
						if ((''.join(map(str,eof)) in ''.join(map(str,decoding)))==True): # if found sign of end stop collect pixels
							print ("decoding...")
							sec = ''.join(decoding[:-14]) # cut eof
							return bin2str(sec)
						else:
							r,g,b=im.getpixel(xy)
							decoding.append(bin(r)[-1])
							if not ((''.join(map(str,eof)) in ''.join(map(str,decoding)))==True): # additional condition, needed because each pixel contain R, G and B components
								decoding.append(bin(g)[-1])
							if not ((''.join(map(str,eof)) in ''.join(map(str,decoding)))==True):
								decoding.append(bin(b)[-1])
				if not ((''.join(map(str,eof)) in ''.join(map(str,decoding)))==True): #check if choosed file have encoded message
					print ("Sorry, this file have no encoded message.")
					break
			else:
				print("Extension must be png!")
		else:
			print ("File doesn't exist. Try again...")

def selection(): # option to encode or decode
	print("1. encode")
	print("2. decode")
	start=1
	while start:
			select=raw_input("Please choose an option:")
			if select=='1':
					proper_name=1
					while proper_name:
						name=raw_input("name of image you want to use: ") # choose image
						if os.path.isfile(name): #check if file exist
							extensions=[".png", ".jpg", "jpeg", ".bmp"] # acceptable extensions
							#if extensions[0] or extensions[1] or extensions[2] or extensions[3] in name:
							secret_message = raw_input("Your message to hide: ")
							secret= (str(str2bin(secret_message))+str(eofb[2::])) # add to secret sing eof
							print ("encoding...")
							bin_sec=[]
							for x in secret:
								bin_sec.append(int(x)) #secret in binary

							for x in range(0,len(eof)):
								bin_sec.append(eof[x])

							encode(xy,name,clear_bit(xy,name,secret),secret) # use function encode to start encoding
							start=0
							proper_name=0
#							else:
#								print("Not acceptable extension, please change another one")
						else:
							print ("File doesn't exist. Try again...")
			elif select=='2':
					print ("decode: ", decode()) # use function decode to decoding
					start=0
			else:
					print("You chooosed wrong option, try again")


def main():
	global eof,eofb, compare, x,y, xy
	eof=[0,0,0,0,0,0,1,1,0,0,0,0,0,0] # sign of end secret
	eofb ='0b00000011000000' # the same in binary format
	#compare = 0b00000000
	compare = 0b11111110 # clear last bit, others remain unchanged
	x=y=0
	xy=(x,y)
	selection() # use function selection to choose option you want to use

tab_r=[]
tab_g=[]
tab_b=[]


if __name__ == "__main__":
	main()
