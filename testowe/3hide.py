from PIL import Image
import binascii
import os.path


class Steg:

	def __init__(self):

		#global eof,eofb, compare, x,y, xy
		self.eof=[0,0,0,0,0,0,1,1,0,0,0,0,0,0] # sign of end secret
		self.eofb ='0b00000011000000' # the same in binary format
		#compare = 0b00000000
		self.compare = 0b11111110 # clear last bit, others remain unchanged
		x=y=0
		xy=(x,y)
		self.tab_r=[]
		self.tab_g=[]
		self.tab_b=[]
		self.selection(xy) # use function selection to choose option you want to use



	def str2bin(self,message): #change string to binary
		self.binary = bin(int(binascii.hexlify(message),16))
		return self.binary[2::]
	def bin2str(self,binary): #change binary to string
		self.message = binascii.unhexlify('%x' % (int('0b'+binary,2)))
		return self.message

	def clear_bit(self,xy):  #clear last bit of this pixel witch will be used to hide secret
		counter=0
		for x in range(0,self.width,10):
			if counter<=(len(self.secret)):
				for y in range(0,self.height,10):
					xy=(x,y)
					r,g,b=self.im.getpixel(xy)
					r=r&self.compare # clears last bit
					g=g&self.compare
					b=b&self.compare
					self.im.putpixel(xy,(r,g,b)) # puts to image changed pixel
					counter+=1
			else:
				break
		return self.im

	def encode(self,xy): # put a proper value of secret to pixels
		z=0
		for x in range(0,self.width,10):
			for y in range(0,self.height,10):
				xy=(x,y)
				r,g,b=self.im.getpixel(xy)
				if z<len(self.secret):	# stops when secred ended
					if int(self.secret[z])==1:
						r+=1
						self.im.putpixel(xy,(r,g,b))
					z+=1
					self.tab_r.append(bin(r)[-1])
				if z<len(self.secret):
					if int(self.secret[z])==1:
						g+=1
						self.im.putpixel(xy,(r,g,b))
					z+=1
					self.tab_g.append(bin(g)[-1])
				if z<len(self.secret):
					if int(self.secret[z])==1:
						b+=1
						self.im.putpixel(xy,(r,g,b))
					z+=1
					self.tab_b.append(bin(b)[-1])
				#if z>=len(secret):
				#	print ("srututu")
				#	break
				if len(self.secret)>(self.width/10 * self.height/10): # check if secret is enough short to fulfil image
					print ("Secret is too long. Please shorten it or chose bigger image.")
					self.selection(xy)
		if ".png" in self.name:
			self.im.save(self.name.replace(".png","-encoded.png"), "PNG") # save image as "name-encoded.png"
		else:
			extension = self.name.split(".")
			print("changing extension from " +str(extension[1])+ " to png")
			self.im.save(self.name.replace("."+extension[1],"-encoded.png"), "PNG")
		self.im.show()

	def decode(self): # function used to decode image
		proper_name=1
		while proper_name:
			imag = input("Name of image: ")
			if os.path.isfile(imag): # check if file exist
				must=".png"
				if must in imag:
					im = Image.open(imag)
					width,height=im.size
					im=im.convert('RGB')
					decoding=[] # contain bits of secret
					for x in range(0,width,10):
						for y in range(0,height,10):
							xy=(x,y)
							if ((''.join(map(str,self.eof)) in ''.join(map(str,decoding)))==True): # if found sign of end stop collect pixels
								print ("decoding...")
								sec = ''.join(decoding[:-14]) # cut eof
								return self.bin2str(sec)
							else:
								r,g,b=im.getpixel(xy)
								decoding.append(bin(r)[-1])
								if not ((''.join(map(str,self.eof)) in ''.join(map(str,decoding)))==True): # additional condition, needed because each pixel contain R, G and B components
									decoding.append(bin(g)[-1])
								if not ((''.join(map(str,self.eof)) in ''.join(map(str,decoding)))==True):
									decoding.append(bin(b)[-1])
					if not ((''.join(map(str,self.eof)) in ''.join(map(str,decoding)))==True): #check if choosed file have encoded message
						print ("Sorry, this file have no encoded message.")
						break
				else:
					print("Extension must be png!")
			else:
				print ("File doesn't exist. Try again...")

	def selection(self,xy): # option to encode or decode
		print("1. encode")
		print("2. decode")
		start=1
		while start:
				select=input("Please choose an option:")
				if select=='1':
						proper_name=1
						while proper_name:
							self.name=input("name of image you want to use: ") # choose image
							if os.path.isfile(self.name): #check if file exist
								im = Image.open(self.name)
								self.width,self.height=im.size
								self.im=im.convert('RGB')
								#extensions=[".png", ".jpg", "jpeg", ".bmp"] # acceptable extensions
								#if extensions[0] or extensions[1] or extensions[2] or extensions[3] in name:
								secret_message = input("Your message to hide: ")
								self.secret= (str(self.str2bin(secret_message))+str(self.eofb[2::])) # add to secret sing eof
								print ("encoding...")
								bin_sec=[]
								for x in self.secret:
									bin_sec.append(int(x)) #secret in binary

								for x in range(0,len(self.eof)):
									bin_sec.append(self.eof[x])
								self.clear_bit(xy)
								self.encode(xy) # use function encode to start encoding
								start=0
								proper_name=0
	#							else:
	#								print("Not acceptable extension, please change another one")
							else:
								print ("File doesn't exist. Try again...")
				elif select=='2':
						print ("decode: ", self.decode()) # use function decode to decoding
						start=0
				else:
						print("You chooosed wrong option, try again")

Steg()
