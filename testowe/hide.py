from PIL import Image
import binascii

class Steg:
	def __init__(self, nazwa):
	#	self.im=im
		im = Image.open(nazwa)
		self.width,self.height=im.size
		self.im=im.convert('RGB')

		self.tab_r=[]
		self.tab_g=[]
		self.tab_b=[]
		self.eof=[0,0,0,0,0,0,1,1,0,0,0,0,0,0] # sign of end secret
		eofb='0b00000011000000'
		self.secret= (str(self.str2bin('usz tam budem KOKOty'))+str(eofb[2::]))
		print ("secret: ", self.secret[:-14])
		self.bin_sec=[]
		for x in self.secret:
			self.bin_sec.append(int(x)) #secret in binary

		for x in range(0,len(self.eof)):
			self.bin_sec.append(self.eof[x])

		self.compare=0b00000000
		#compare=0b11111110 # clear last bit, others remain unchanged

		x=y=0
		xy=(x,y)
		self.clear_bit(xy)
		self.encode(xy)
		print ("decode: ", self.decode())
		im.save("decoded.png", "PNG")
		im.show()


	def str2bin(self,message):
		self.binary = bin(int(binascii.hexlify(message),16))
		return self.binary[2::]

	def bin2str(self,binary):
		self.message = binascii.unhexlify('%x' % (int('0b'+binary,2)))
		return self.message

	def clear_bit(self,xy):  #clear last bit of this pixel witch will be used to hide secret
		counter=0
		for x in range(0,self.width,10):
			if counter<=(len(self.secret)):	# + 13 for sign_of_end
				for y in range(0,self.height,10):
					xy=(x,y)
					r,g,b=self.im.getpixel(xy)
					r=r&self.compare
					g=g&self.compare
					b=b&self.compare
					self.im.putpixel(xy,(r,g,b))
					counter+=1
			else:
				break

	def encode(self,xy): # put a proper value of secret to pixels
		z=0
		for x in range(0,self.width,10):
			for y in range(0,self.height,10):
				xy=(x,y)
				r,g,b=self.im.getpixel(xy)
				if z<len(self.secret):
					if int(self.secret[z])==1:
						r+=1
						self.im.putpixel(xy,(r,g,b))
					z+=1
					self.tab_r.append(bin(r)[-1])
				if z<len(self.secret):
					if int(self.secret[z])==1:
						g+=1
						self.im.putpixel(xy,(r,g,b))
					z+=1
					self.tab_g.append(bin(g)[-1])
				if z<len(self.secret):
					if int(self.secret[z])==1:
						b+=1
						self.im.putpixel(xy,(r,g,b))
					z+=1
					self.tab_b.append(bin(b)[-1])
				if z>=len(self.secret):
					break


	def decode(self): # function used to decode image
		decoding=[]
		for x in range(0,self.width,10):
			for y in range(0,self.height,10):
				xy=(x,y)
				if ((''.join(map(str,self.eof)) in ''.join(map(str,decoding)))==True): # if found sign of end stop collect pixels
					print ("decoding...")
					#print ("len decoding: ", len(decoding))
					#checking_value=len(secret)
					#ok = ''.join(decoding[:-(len(eof)+1)])
					ok = ''.join(decoding[:-14])
					#print ("len decodingOK ", len(ok))
					#print ("read value: "+str(ok))
					return self.bin2str(ok) # cut eof signs
				else:
					r,g,b=self.im.getpixel(xy)
					decoding.append(bin(r)[-1])
					if not ((''.join(map(str,self.eof)) in ''.join(map(str,decoding)))==True):
						decoding.append(bin(g)[-1])
					if not ((''.join(map(str,self.eof)) in ''.join(map(str,decoding)))==True):
						decoding.append(bin(b)[-1])


	###################

	#print ("len eof: ",len(eofb))
	def show_info(self): # information shows in console
		print ("secret in binary: "+ str(self.bin_sec))
		print ("secret normally: "+ str(self.secret[:-14]))
		print ("length of secret: "+ str(len(self.secret[:-14])))
		print ("encoded secret:" +str(self.bin2str(self.secret[:-14])))


	#tu=("str2bin z aktualnego sekretu: "+str(str2bin('usz tam')))
	#print ("tu: ", tu)
	#print (bin2str(tu))

	def main(self):

		pass


	def show_rgb(self):
		print ("r: "+str(self.tab_r))
		print ("..................")
		print ("g: "+str(self.tab_g))
		print ("..................")
		print ("b: "+str(self.tab_b))
	#show_info()
	#show_rgb()


Steg('view2.png')

##############################################################################

'''
temp_red=[]
temp_green=[]
temp_blue=[]


# to check last bit of used pixels
for x in range(0,len(val_sec),3):
	temp_red.append(val_sec[x])
print "temp red:" +str(temp_red)


for x in range(1,len(val_sec),3):
	temp_green.append(val_sec[x])
print "temp green:" +str(temp_green)

for x in range(2,len(val_sec),3):
	temp_blue.append(val_sec[x])
print "temp blue:" +str(temp_blue)
'''


'''
def check(xy):
	r,g,b=im.getpixel(xy)
	for x in range(0,width,20):
		for y in range(0,height,20):
			print bin(r)[2::]
			print bin(g)[2::]
			print bin(b)[2::]
			xy=(x,y)
			r,g,b=im.getpixel(xy)

tab_r_bef=[]
'''


'''
def encode(xy):
	encoded=[]
#	counter=0
	r,g,b=im.getpixel(xy)
	if len(encoded)<len(secret):
		for x in range(0,width,20):
			for y in range(0,height,20):
#				if counter<=len(secret):
				xy=(x,y)
				r,g,b=im.getpixel(xy)
				print (str(r))
#				counter+=1
            encoded.append(r)
            encoded.append(g)
            encoded.append(b)
'''
